package ie.ucd.luggage;

public class Laptop implements Item {
	
	//Laptop class
	//Author: Niall Henry

	private String type;
	private double weight;
	
	public Laptop(String type, double weight) { // Laptop constructor
		this.weight = weight;
		this.type = type;
	}
	@Override					//Overridden methods
	public String getType() {
		return type;
	}

	@Override
	public double getWeight() {
		return weight;
	}

	@Override
	public boolean isDangerous() {
		return false;
	}

}
