package ie.ucd.luggage;

import java.util.ArrayList;
import java.util.Scanner;

public class SafeLuggage extends Luggage {

	//Safe luggage class 
	//Author: Niall Henry
	
	private int password = 1234;
	private double bagWeight;
	private double MaxWeight;
	private static Scanner scan = new Scanner(System.in);

	
	public SafeLuggage(double bagWeight, double MaxWeight) { //SafeLuggage constructor
		this.bagWeight = bagWeight;
		this.MaxWeight = MaxWeight;	
	}
	@Override							//Overridden methods
	public double getBagWeight() {
		return bagWeight;
	}

	@Override
	public double getMaxWeight() {
		return MaxWeight;
	}
	
	public void removeItem(int index){			//remove item method which asks for password
		System.out.println("ENTER PASSWORD:");
		double password = scan.nextDouble();
		if (password == this.password)
			super.removeItem(index);
		
		else {
			System.out.println("Incorrect Password");
		}
	}
	
	public void add(Item item){
		System.out.println("ENTER PASSWORD");
		double password = scan.nextDouble();
		if (password == this.password)
			super.add(item);
		else {
			System.out.println("Incorrect Password");
		}
	}

}
