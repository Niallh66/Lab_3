package ie.ucd.luggage;

public class Suitcase extends Luggage {
	
	//Author: Niall Henry
	//Luggage class

	private double bagWeight;
	private double MaxWeight;
	
	public Suitcase(double bagWeight, double MaxWeight) { //Suitcase Constructor
		this.bagWeight = bagWeight;
		this.MaxWeight = MaxWeight;
	}
	
	@Override							//Overridden methods
	public double getBagWeight() {
		return bagWeight;
	}

	@Override
	public double getMaxWeight() {
		return MaxWeight;
	}

}
