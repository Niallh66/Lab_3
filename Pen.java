package ie.ucd.luggage;

public class Pen implements Item {
	//Item class

	private double weight;
	protected String type;
	
	public Pen(String type, double weight) { //Pen constructor
		this.type = type;
		this.weight = weight;
	}
	
	@Override					//Overridden methods
	public String getType() {
		return type;
	}

	@Override
	public double getWeight() {
		return weight;
	}

	@Override
	public boolean isDangerous() {
		return false;
	}

}
