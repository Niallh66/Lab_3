package ie.ucd.luggage;

import java.util.List;

public class Main {
	
	//Main used to test the functionality if the code written
	

	public static void main(String[] args) {
		
		double bagWeight = 20.0;								//Initializing variables
		double MaxW = 25.0;
		Suitcase mysuitcase = new Suitcase(bagWeight,MaxW);		//Creating objects
		SafeLuggage mySlug = new SafeLuggage(bagWeight,MaxW);
		
		double SlugW = mySlug.getBagWeight();					//Creating weights
		double SlugMW = mySlug.getMaxWeight();
		
		double SuitW = mysuitcase.getBagWeight();
		double SuitMW = mysuitcase.getMaxWeight();
		
		Item Bomb = new Bomb("Big Bom",5.0);					//Creating item objects
		Item Pen = new Pen("Bic", 0.2);
		Item DPen = new DesignerPen("Fancy", 0.5);
		Item Laptop = new Laptop("Dell", 24.0);
		
		mySlug.add(Bomb);										//Adding objects
		mySlug.add(Pen);
		mySlug.add(Laptop);
		
		mysuitcase.add(Bomb);
		mysuitcase.add(DPen);
		mysuitcase.add(Laptop);
		
		double We = mySlug.getWeight();							//Finding weights
		double SWe = mysuitcase.getWeight();
		
		if(We>SlugMW) {											//User prints
			System.out.println("Bag is too heavy, checking bag");
		}
		else if(SWe>SuitMW) {
			System.out.println("Bag is too heavy, checking bag");
		}
		
		List<Item> i = mySlug.getContents();					//Getting contents
		List<Item> j = mysuitcase.getContents();
 		
		for(Item m: i) {										//Loop to look through items
			if(m.isDangerous()) {
				System.out.println("Dangerous Item Added");
				String type = m.getType();
				System.out.println("Found dangerous item: " + type);
				mySlug.removeItem(i.indexOf(m));
				System.out.println("Removing dangerous item: " + type + "\nYou are going to jail\n");	
				break;
			}
			else
				System.out.println("Bag is safe");
		}
		for(Item z: j) {
			if(z.isDangerous()) {
				System.out.println("Dangerous Item Added");
				String type1 = z.getType();
				System.out.println("Found dangerous item: " + type1);
				mysuitcase.removeItem(j.indexOf(z));
				System.out.println("Removing dangerous item: " + type1 + "\nYou are going to jail\n");
				break;		
			}
			else
				System.out.println("Bag is safe");
		}
	}
}
