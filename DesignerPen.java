package ie.ucd.luggage;

public class DesignerPen extends Pen {
	
	//DesignerPen class
	
	private String Brand;
	
	public DesignerPen(String Brand, double weight) { //DesignerPen constructor
		super(Brand, weight);
	}
		
	public String getType() {
		return Brand;
	}	
		
}
	
