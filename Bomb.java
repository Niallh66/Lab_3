package ie.ucd.luggage;

public class Bomb implements Item {
	
	//Bomb item class
	
	private String type;
	private double weight;
	
	public Bomb(String type, double weight) { //Bomb constructor
		this.weight = weight;
		this.type = type;
	}

	@Override					//Overridden methods
	public String getType() {
		return type;
	}

	@Override
	public double getWeight() {
		return weight;
	}

	@Override
	public boolean isDangerous() {
		return true;
	}

}
